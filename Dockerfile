FROM centos:6.6
MAINTAINER bingo wxdlong@qq.com


ARG MASTER=true
ENV PGDATA /var/lib/pgsql/9.6/data
ENV PG_HOME /usr/pgsql-9.6/
ENV PG_PASS /var/lib/pgsql/.pgpass
ENV PATH ${PG_HOME}/bin:$PATH
ENV PASSWORD replic
#install packages
RUN yum install -y yum-plugin-ovl tcpdump openssh-server openssh-clients vim iproute2 iputils-ping \
    https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-6-x86_64/pgdg-centos96-9.6-3.noarch.rpm 
RUN yum install -y postgresql96 postgresql96-server

#passwordless ssh
RUN mkdir /var/run/sshd; \
    echo 'root:root' | chpasswd; \
    sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config; \
    ssh-keygen -q -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key; \
    ssh-keygen -q -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key; \
    # SSH login fix. Otherwise user is kicked off after login
    sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd


RUN echo "postgres" > ${PG_PASS}; \
    echo "*:5432:replication:replic:${PASSWORD}" >> ${PG_PASS} ; \
    chown postgres:postgres ${PG_PASS}; \
    chmod 0600 ${PG_PASS}; \
    echo "export PG_HOME=${PG_HOME}" > /var/lib/pgsql/.pgsql_profile ; \
    echo "export PATH=${PG_HOME}/bin:${PATH}" >> /var/lib/pgsql/.pgsql_profile ; \
    echo 'export PS1="\[\e[32;1m\][\[\e[33;1m\]\u\[\e[31;1m\]@\[\e[33;1m\]\h \[\e[36;1m\]\w\[\e[32;1m\]]\[\e[34;1m\]\$ \[\e[0m\]"' >> /var/lib/pgsql/.pgsql_profile ; \
    chown postgres:postgres /var/lib/pgsql/.pgsql_profile ;
  
 
COPY docker-entrypoint.sh /usr/local/bin/
COPY resource/postgresql.conf /home

ENTRYPOINT ["docker-entrypoint.sh"]

# CMD ["/usr/sbin/sshd", "-D"]