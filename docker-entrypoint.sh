#!/bin/bash

echo "start sshd service" | tee -a /var/log/pg.log

echo "PGDATA: ${PGDATA}"
echo "MASTER: ${MASTER}"

if [ ${pg_type} == "master" ]; then
	{
		su - postgres -c "/usr/pgsql-9.6/bin/initdb -E UTF-8 --locale=en_US.UTF-8  -D ${PGDATA} -U postgres --pwfile=${PG_PASS}" | tee -a /var/log/pg.log
		echo "initdb postgres " | tee -a /var/log/pg.log
		echo "host    replication     ${PASSWORD}        samenet            md5" >>${PGDATA}/pg_hba.conf
		cp -rf /home/postgresql.conf ${PGDATA}
		su - postgres -c "/usr/pgsql-9.6/bin/pg_ctl -D ${PGDATA} start >/dev/null "
		echo "pg_ctl start" | tee -a /var/log/pg.log
		while [ $(ss -atnp | grep *:5432 | grep -q postgres || echo true) ]; do
			echo "waiting postgres start " | tee -a /var/log/pg.log
			sleep 1
		done
		su - postgres -c "psql -c \"create role replic with login replication encrypted password '${PASSWORD}' \"" | tee -a /var/log/pg.log
		echo "create role replic with login replication encrypted password '${PASSWORD}'" | tee -a /var/log/pg.log

		echo "standby_mode = 'on'" >${PGDATA}/recovery.done
		echo "primary_conninfo = 'user=replic password=${PASSWORD} host=pgslave port=5432 sslmode=prefer sslcompression=1 krbsrvname=postgres'" >>${PGDATA}/recovery.done
		echo "recovery_target_timeline = 'latest'" >>${PGDATA}/recovery.done
		echo "trigger_file = '/var/lib/pgsql/master' " >>${PGDATA}/recovery.done
		echo "restore_command = 'cp /var/lib/pgsql/9.6/backups/%f %p' " >>${PGDATA}/recovery.done
		echo "archive_cleanup_command = '/usr/pgsql-9.6/bin/pg_archivecleanup -d /var/lib/pgsql/9.6/backups/ %r >> /var/lib/pgsql/cleanup.log' " >>${PGDATA}/recovery.done
		chmod 0644 ${PGDATA}/recovery.done
		chown postgres:postgres ${PGDATA}/recovery.done
	}

else {
	while [ true ]; do
		echo "waiting postgres start " | tee -a /var/log/pg.log
		if >/dev/tcp/pgmaster/5432; then
			break
		fi
		sleep 2
	done
	su - postgres -c " /usr/pgsql-9.6/bin/pg_basebackup -h pgmaster -U replic -D ${PGDATA} -X stream -P -R" >>/var/log.pg.log
	echo "pg_basebackup postgres " | tee -a /var/log/pg.log
	echo "recovery_target_timeline = 'latest'" >>${PGDATA}/recovery.conf
	echo "trigger_file = '/var/lib/pgsql/master'" >>${PGDATA}/recovery.conf
	echo "restore_command = 'cp /var/lib/pgsql/9.6/backups/%f %p' " >>${PGDATA}/recovery.conf
	echo "archive_cleanup_command = '/usr/pgsql-9.6/bin/pg_archivecleanup -d /var/lib/pgsql/9.6/backups/ %r >> /var/lib/pgsql/cleanup.log' " >>${PGDATA}/recovery.conf
	su - postgres -c "/usr/pgsql-9.6/bin/pg_ctl -D ${PGDATA} start"
	echo "pg_ctl start" | tee -a /var/log/pg.log
}

fi

/usr/sbin/sshd -D

exec "$@"
